#include <but/benchmark.hpp>
#include <but/random.hpp>
#include <but/but.hpp>
#include <but/utils.hpp>

int random_loop()
{
    // int amount = basket::but::random();
    int amount = 1000000;
    int a = 0;
    for (int index = 0; index < amount; index++)
    {
        if (index % 2 == 0)
        {
            a += index;
        }
        else 
        {
            a -= index;
        }
    }

    return a;
}

void test_sleep()
{
    basket::but::sleep(1000);
}

int main()
{
    BTEST(3 == 5);
    BTEST(3 == 3);
    BBENCH(random_loop);
    BBENCH(test_sleep);
}