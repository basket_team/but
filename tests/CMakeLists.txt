cmake_minimum_required(VERSION 3.14)

project(butlib_tests)



add_executable(unit_test tests.cpp)
target_link_libraries(unit_test PRIVATE "${BK_but_LIBS}")
# target_link_libraries(unit_test PRIVATE "${BK_bklog_LIBS}")
# target_link_libraries(unit_test PRIVATE bklog)
target_include_directories(unit_test PRIVATE "${BK_but_INCLUDES}")
set_target_properties(unit_test PROPERTIES CXX_STANDARD 17)
set_property(TARGET unit_test PROPERTY RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/bin")

# target_include_directories(unit_test PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../)