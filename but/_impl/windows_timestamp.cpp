#include "windows_timestamp.hpp"

#include <bklog/bklog.h>

#include <Windows.h> 

namespace basket::but
{

extern bklog* _but_log;


timestamp::timestamp(const char* p_name)
{
    _name = p_name;
    QueryPerformanceFrequency(&_frequency);
    QueryPerformanceCounter(&_start);
}

timestamp::~timestamp()
{
    LARGE_INTEGER _end;
    QueryPerformanceCounter(&_end);

    LONGLONG elapsed = _end.QuadPart - _start.QuadPart;
    double seconds = (double)elapsed / _frequency.QuadPart;
    bklog_print(_but_log, BKLOG_LEVEL_INFO, "%s time(s): %f", _name, seconds);
}


}
