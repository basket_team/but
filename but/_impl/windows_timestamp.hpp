#ifndef BK__IMPL_WINDOWS_TIMESTAMP_HPP
#define BK__IMPL_WINDOWS_TIMESTAMP_HPP

#include "../defs.hpp"

#include <Windows.h>

namespace basket::but
{
    class BUT_API timestamp
    {
        private:
            LARGE_INTEGER _start;
            LARGE_INTEGER _frequency;
            const char* _name; // The function name should not be longer than this.

        public:
            timestamp(const char* p_name);
            ~timestamp();
    };
}

#endif