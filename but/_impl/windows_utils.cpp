#include "../utils.hpp"

#include "../defs.hpp"

#include <Windows.h>

namespace basket::but
{
    BUT_API void sleep(unsigned long int p_milliseconds)
    {
        Sleep(p_milliseconds);
    }

    BUT_API int random()
    {
        return rand();
    }
}