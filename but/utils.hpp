#ifndef BK_BUT_UTILS_HPP
#define BK_BUT_UTILS_HPP

#include "defs.hpp"

namespace basket::but
{
    BUT_API void sleep(unsigned long int p_milliseconds);
    BUT_API int random();
}

#endif