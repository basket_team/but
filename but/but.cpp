#include "but.hpp"

#include <bklog/bklog.h>

namespace basket::but
{
    bklog* _but_log = ::bklog_create("BUT");
    

    BUT_API bool check(bool p_coond, const char* p_coond_name)
    {

        bklog_print(_but_log, BKLOG_LEVEL_INFO, "testing (%s). Result: %s", p_coond_name, p_coond ? "success": "fail");
        return p_coond;
    }
}