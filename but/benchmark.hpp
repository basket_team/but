#ifndef BUT_BENCHMARK_HPP
#define BUT_BENCHMARK_HPP


#include "defs.hpp"
#include "timestamp.hpp"

namespace basket::but
{
    BUT_API void _print_result(unsigned long long result);
}

#define BBENCH(function) \
    {\
        basket::but::timestamp ts(#function);\
        function();\
    }\

#endif