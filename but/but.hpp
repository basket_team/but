#ifndef BUT_BUT_HPP
#define BUT_BUT_HPP

#include "defs.hpp"



namespace basket::but
{
    BUT_API bool check(bool p_cond, const char* p_cond_name);
}

#define BTEST(coond) \
    if (::basket::but::check(coond, #coond))\
        (void)0;\
    else\
        (void)0

#endif