#ifndef BUT_DEFS_H
#define BUT_DEFS_H

#ifdef _WIN32
    #define BUT_DECL __decl
    #define BUT_STDCALL __cdecl
    #ifdef BUT_SHARED
        #ifdef BUT_EXPORT
            #define BUT_API __declspec(dllexport)
        #else
            #define BUT_API __declspec(dllimport)
        #endif
    #else
        #define BUT_API
    #endif
#elif UNIX
    #define BUT_DECL 
    #define BUT_API
#endif

#endif