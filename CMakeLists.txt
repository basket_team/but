cmake_minimum_required(VERSION 3.14)

project(but)


option(TESTS_ENABLED "Enable tests" ON)

option(BUILD_STATIC_LIB "Static build" OFF)
# set(BUILD_STATIC_LIB ON)

if(NOT BUILD_STATIC_LIB)
    message("Building dynamic lib...")
    set(BUILD_TYPE SHARED)
else()
    message("Building static lib...")
    set(BUILD_TYPE STATIC)
endif()

add_subdirectory(deps/bklog)

set(BUT_SOURCES
    ${CMAKE_CURRENT_SOURCE_DIR}/but/benchmark.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/but/_impl/windows_timestamp.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/but/_impl/windows_utils.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/but/random.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/but/but.cpp
)

add_library(butlib ${BUILD_TYPE} "${BUT_SOURCES}")


set_target_properties(butlib PROPERTIES CXX_STANDARD 17)
target_compile_definitions(butlib PRIVATE BUT_EXPORT)
if(NOT BUILD_STATIC_LIB)
    target_compile_definitions(butlib PUBLIC BUT_SHARED)
endif()
target_include_directories(butlib PRIVATE "${BK_bklog_INCLUDES}")
target_link_libraries(butlib PRIVATE "${BK_bklog_LIBS}")
set_property(TARGET butlib PROPERTY LIBRARY_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/bin")
set_property(TARGET butlib PROPERTY RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/bin")
set_property(TARGET butlib PROPERTY ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/bin")
set(BK_but_INCLUDES "${CMAKE_CURRENT_SOURCE_DIR}" CACHE INTERNAL "")
set(BK_but_LIBS "butlib" CACHE INTERNAL "")


# if(TESTS_ENABLED)
    add_subdirectory(tests)
# endif()
